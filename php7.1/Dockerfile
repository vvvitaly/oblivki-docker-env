FROM php:7.1-fpm

ARG workdir

WORKDIR $workdir

# Install Additional dependencies
RUN apt-get update && apt-get install -y \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    git \
    zlib1g-dev \
    libicu-dev \
    g++ \
    libc-client-dev \
    libkrb5-dev \
    libxml2-dev \
    libxml2-dev \
    libmemcached-dev \
    iproute2 \
    zip unzip \
    liblz4-1 liblz4-dev \
    procps \
    && rm -r /var/lib/apt/lists/* \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install pdo_mysql mysqli zip bcmath gd intl sockets exif pcntl \
    && pecl install xdebug-2.6.0 \
    && pecl install redis-4.0.1 \
    && docker-php-ext-enable redis xdebug sockets

ENV XDEBUGINI_PATH=/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "zend_extension="`find /usr/local/lib/php/extensions/ -iname 'xdebug.so'` > $XDEBUGINI_PATH
COPY xdebug.ini /tmp/xdebug.ini
RUN cat /tmp/xdebug.ini >> $XDEBUGINI_PATH
RUN echo "xdebug.remote_host="`/sbin/ip route|awk '/default/ { print $3 }'` >> $XDEBUGINI_PATH

RUN curl -fsSL 'https://github.com/tarantool/tarantool-php/archive/0.3.2.tar.gz' -o tarantool.tar.gz \
    && mkdir -p tarantool \
    && tar -xf tarantool.tar.gz -C tarantool --strip-components=1 \
    && rm tarantool.tar.gz \
    && ( \
        cd tarantool \
        && phpize \
        && ./configure \
        && make -j "$(nproc)" \
        && make install \
    ) \
    && rm -r tarantool \
    && docker-php-ext-enable tarantool

RUN curl -sS -o /tmp/icu.tar.gz -L https://github.com/unicode-org/icu/releases/download/release-64-2/icu4c-64_2-src.tgz \
    && tar -zxf /tmp/icu.tar.gz -C /tmp \
    && cd /tmp/icu/source \
    && ./configure --prefix=/usr/local \
    && make \
    && make install

RUN docker-php-ext-configure intl --with-icu-dir=/usr/local \
    && docker-php-ext-install intl

RUN curl -fsSL 'https://github.com/kjdev/php-ext-lz4/archive/0.3.5.tar.gz' -o lz4-ext.tar.gz \
    && mkdir -p lz4-ext \
    && tar -xf lz4-ext.tar.gz -C lz4-ext --strip-components=1 \
    && rm lz4-ext.tar.gz \
    && ( \
        cd lz4-ext \
        && phpize \
        && ./configure --with-lz4-includedir=/usr \
        && make -j "$(nproc)" \
        && make install \
    ) \
    && rm -r lz4-ext \
    && docker-php-ext-enable lz4

RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"

# Install PHP Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

EXPOSE 9000
CMD ["php-fpm"]
